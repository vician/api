#!/bin/bash

someerror=0

function counttest() {
	url=$1
	name=$2
	if [ ! -f $url ]; then
		bashcount=$(wget -qO- $url | grep "BEGIN:VEVENT" | wc -l)
	else
		bashcount=$(cat $url | grep "BEGIN:VEVENT" | wc -l)
	fi
	phpcount=$(php docs/ics/eventcount.php -- url=$url 2>stderr.txt)
	if [ $(wc -l stderr.txt | awk '{print $1}') -ne 0 ]; then
		echo "ERROR: eventcount.php Some PHP WARNING/NOTICE/ERROR!"
		cat stderr.txt
		exit 1
	fi

	if [ $bashcount -ne $phpcount ]; then
		echo "ERROR: bashcount ($bashcount) and phpcount ($phpcount) are not the same for $name!"
		someerror=1
	fi

	echo "Eventcount $name found $bashcount = $phpcount events!"
}

if [ ! -f IrishHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/en.irish%23holiday%40group.v.calendar.google.com/public/basic.ics" -O IrishHoliday.ics
fi
if [ ! -f CzechHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/cs.czech%23holiday%40group.v.calendar.google.com/public/basic.ics"  -O CzechHoliday.ics
fi
if [ ! -f ChristianHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/en.christian%23holiday%40group.v.calendar.google.com/public/basic.ics" -O ChristianHoliday.ics
fi
if [ ! -f MoonPhases.ics ]; then
	wget "https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics" -O MoonPhases.ics
fi

counttest ./IrishHoliday.ics IrishHoliday
counttest ./CzechHoliday.ics CzechHoliday
counttest ./ChristianHoliday.ics ChristianHoliday
counttest https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics MoonPhases
counttest https://calendar.google.com/calendar/ical/prqq6aojjgsgsseqbpvkclsk7c%40group.calendar.google.com/public/basic.ics LiturgyCalendar19
counttest https://calendar.google.com/calendar/ical/vician.cz_82klsc79ujuh8emsba085vam90%40group.calendar.google.com/public/basic.ics IndividualDailyRosary

exit $someerror
