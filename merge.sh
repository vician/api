#!/bin/bash

someerror=0

function counttest() {
	urls=$@
	eachcount=0
	params=""
	for url in ${urls[@]}; do
		count=$(php docs/ics/eventcount.php -- url=$url 2>stderr.txt)
		if [ $(wc -l stderr.txt | awk '{print $1}') -ne 0 ]; then
			echo "ERROR: eventcount.php Some PHP WARNING/NOTICE/ERROR!"
			cat stderr.txt
			exit 1
		fi
		eachcount=$(expr $eachcount + $count)
		echo -n " + $count"
		params="${params}url[]=$url&"
	done
	echo " = $eachcount"

	php docs/ics/merge.php -- $params > merged.ics 2>stderr.txt
	if [ $(wc -l stderr.txt | awk '{print $1}') -ne 0 ]; then
		echo "ERROR: merge.php Some PHP WARNING/NOTICE/ERROR!"
		cat stderr.txt
		exit 1
	fi
	paramscount="${params}count"
	mergedcount=$(php docs/ics/merge.php -- $paramscount)
	allcount=$(php docs/ics/eventcount.php -- url=merged.ics)
	if [ $(wc -l stderr.txt | awk '{print $1}') -ne 0 ]; then
		echo "ERROR: eventcount.php Some PHP WARNING/NOTICE/ERROR!"
		cat stderr.txt
		exit 1
	fi
	if [ $eachcount -ne $allcount ]; then
		echo "ERROR: eachcount ($eachcount) and allcount ($allcount) are not the same!"
		someerror=1
	fi
	echo "$mergedcount"
	if [ $mergedcount -ne $allcount ]; then
		echo "ERROR: mergedcount ($mergedcount) and allcount ($allcount) are not the same!"
		someerror=1
	fi

	begin_vcalendar=$(grep "BEGIN:VCALENDAR" merged.ics | wc -l | awk '{print $1}')
	if [ $begin_vcalendar -ne 1 ]; then
		echo "ERROR: begin_vcalendar == $begin_vcalendar!"
		exit 1
	fi
	end_vcalendar=$(grep "END:VCALENDAR" merged.ics | wc -l | awk '{print $1}')
	if [ $end_vcalendar -ne 1 ]; then
		echo "ERROR: begin_vcalendar == $end_vcalendar!"
		exit 1
	fi
	head -n 1 merged.ics | grep "BEGIN:VCALENDAR" 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: first line is not BEGIN:VCALENDAR!"
		exit 1
	fi
	tail -n 1 merged.ics | grep "END:VCALENDAR" 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: last line is not END:VCALENDAR!"
		exit 1
	fi
	xwrcalname=$(grep "X-WR-CALNAME" merged.ics | wc -l | awk '{print $1}')
	if [ $xwrcalname -gt 1 ]; then
		echo "ERROR: Multiple X-WR-CALNAME $xwrcalname!"
		exit 1
	fi
	xwrtimezone=$(grep "X-WR-TIMEZONE" merged.ics | wc -l | awk '{print $1}')
	if [ $xwrtimezone -gt 1 ]; then
		echo "ERROR: Multiple X-WR-TIMEZONE $xwrtimezone!"
		exit 1
	xwrcaldesc=$(grep "X-WR-CALDESC" merged.ics | wc -l | awk '{print $1}')
	if [ $xwrcaldesc -gt 1 ]; then
		echo "ERROR: Multiple X-WR-CALDESC $xwrcaldesc!"
		exit 1
	fi
	fi

	echo "Eventcount $eachcount = $allcount = $mergedcount events!"
}

if [ ! -f IrishHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/en.irish%23holiday%40group.v.calendar.google.com/public/basic.ics" -O IrishHoliday.ics
fi
if [ ! -f CzechHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/cs.czech%23holiday%40group.v.calendar.google.com/public/basic.ics"  -O CzechHoliday.ics
fi
if [ ! -f ChristianHoliday.ics ]; then
	wget "https://calendar.google.com/calendar/ical/en.christian%23holiday%40group.v.calendar.google.com/public/basic.ics" -O ChristianHoliday.ics
fi
if [ ! -f MoonPhases.ics ]; then
	wget "https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics" -O MoonPhases.ics
fi
counttest ./IrishHoliday.ics ./IrishHoliday.ics
counttest ./IrishHoliday.ics ./CzechHoliday.ics
counttest ./IrishHoliday.ics ./CzechHoliday.ics ./ChristianHoliday.ics
counttest https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics
counttest https://calendar.google.com/calendar/ical/prqq6aojjgsgsseqbpvkclsk7c%40group.calendar.google.com/public/basic.ics https://calendar.google.com/calendar/ical/vician.cz_82klsc79ujuh8emsba085vam90%40group.calendar.google.com/public/basic.ics
exit $someerror
