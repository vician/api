<?php


if (isset($argc)) {
	if($argc>1) {
		parse_str(implode('&',array_slice($argv, 1)), $_GET);
	}
}

if (!isset($_GET['url'])) {
	die('No input URsL!');
}

$is_only_count = False;
if (isset($_GET['count'])) {
	$is_only_count = True;
} else {
	header('Content-Type: text/calendar');
}

$ics_index = 1;
$eventcount = 0;
foreach ($_GET['url'] as $url) {
	$file = file_get_contents($url);

	$rows = explode("\n", $file);

	$is_event = False;
	foreach($rows as $row => $data) {
		if (preg_match('/\BEGIN:VEVENT\b/', $data)) {
			if ($is_event) {
				die("Invalid ICS, second match of event being!");
			}
			$is_event = True;
			$eventcount++;
		}
		if (preg_match('/\END:VCALENDAR\b/', $data)) {
			if ($is_event) {
				die("Invalid ICS, second match of event being!");
			}
			if ($ics_index == count($_GET['url'])) {
				if ($data != "") {
					if (!$is_only_count) {
						print($data."\n");
					}
				}
			}
			continue;
		}
		if ($ics_index == 1 or $is_event) {
			if (!$is_only_count) {
				if ($data != "") {
					print($data."\n");
				}
			}
		}
		if (preg_match('/\END:VEVENT\b/', $data)) {
			if (!$is_event) {
				die("Invalid ICS, not event start!");
			}
			$is_event = False;
		}
	}
	if ($is_event) {
		die("Invalid ICS, no events end!");
	}
	$ics_index++;
}
if ($is_only_count) {
	print_r($eventcount);
}
?>
