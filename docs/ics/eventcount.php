<?php

if (isset($argc)) {
	if($argc>1) {
		parse_str(implode('&',array_slice($argv, 1)), $_GET);
	}
}

if (!isset($_GET['url'])) {
	die('No input URL!');
}

$file = file_get_contents($_GET['url']);

$rows = explode("\n", $file);

$is_event = False;
$eventcount = 0;
foreach($rows as $row => $data) {
	if (preg_match('/\BEGIN:VEVENT\b/', $data)) {
		if ($is_event) {
			die("Invalid ICS, second match of event being!");
		}
		$is_event = True;
		$eventcount++;
	}
	if (preg_match('/\END:VEVENT\b/', $data)) {
		if (!$is_event) {
			die("Invalid ICS, not event start!");
		}
		$is_event = False;
	}
}
if ($is_event) {
	die("Invalid ICS, no events end!");
}

print_r($eventcount);

?>
