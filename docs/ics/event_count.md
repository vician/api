
<https://ics.vician.dev/eventcount.php>

_Count number of events in given ICS remote file._

## Parameters

- **url**: URL of your remote ICS file for counting events.


## Example usage

## URLs

<https://ics.vician.dev/eventcount.php?url=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics>


	https://ics.vician.dev/eventcount.php?url=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics
