
<https://ics.vician.dev/merge.php>

_Merge multiple ICS remote files to one and export it._

## Parameters

- **url**: URL of your remote ICS file for counting events. Multiple times, so it needs to be defined as `?url[]=...&url[]...&...`

## Example usage

<https://ics.vician.dev/merge.php?url[]=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics&url[]=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics>

	https://ics.vician.dev/merge.php?url[]=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics&url[]=https://calendar.google.com/calendar/ical/ht3jlfaac5lfd6263ulfh4tql8%40group.calendar.google.com/public/basic.ics
