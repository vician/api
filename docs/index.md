## ICS

Set of PHP scripts for working with ICS files.

Created by Martin Vician <https://vician.net>

## Building status

[![](https://gitlab.com/vician/api/badges/master/build.svg)](https://gitlab.com/vician/api)
