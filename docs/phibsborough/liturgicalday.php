<?php

$apidate = date("Ymd");
if (isset($_GET['date'])) {
	$apidate = $_GET['date'];
}

$universalis = file_get_contents('https://universalis.com/europe.ireland/'.$apidate.'/today.htm');
//echo $universalis;
//$html = str_get_html($universalis);
$dom = new DomDocument();
$dom->loadHTML($universalis);
$xpath = new DOMXpath($dom);

$heading=parseToArray($xpath,'feastname');

//print_r($heading);

foreach ($heading as $item) {
	if(strlen($item) > 2) {
		print_r($item);
	}
}

function parseToArray($xpath,$class)
{
    $xpathquery="//span[@id='".$class."']";
    $elements = $xpath->query($xpathquery);

    if (!is_null($elements)) {  
        $resultarray=array();
        foreach ($elements as $element) {
            $nodes = $element->childNodes;
            foreach ($nodes as $node) {
              $resultarray[] = $node->nodeValue;
            }
        }
        return $resultarray;
    }
}
?>
